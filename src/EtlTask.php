<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\KeyMap\KeyMapInterface;

/**
 * Implementation of operations for a full ETL process.
 *
 * @package Soong\Task
 */
class EtlTask extends Task implements EtlTaskInterface
{

    /**
     * {@inheritdoc}
     */
    public function getKeyMap() : ?KeyMapInterface
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return null;
        }
        /** @var \Soong\Extractor\ExtractorInterface $extractor */
        $extractor = new $taskConfiguration['extract']['class']($taskConfiguration['extract']['configuration']);
        $extractorKeys = $extractor->getKeyProperties();

        /** @var \Soong\Loader\LoaderInterface $loader */
        $loader = new $taskConfiguration['load']['class']($taskConfiguration['load']['configuration']);
        $loaderKeys = $loader->getKeyProperties();

        $keyMapConfiguration = $taskConfiguration['key_map']['configuration'] ?? [];
        $keyMapConfiguration = array_merge(
            [KeyMapInterface::CONFIGURATION_EXTRACTOR_KEYS => $extractorKeys],
            $keyMapConfiguration
        );
        $keyMapConfiguration = array_merge(
            [KeyMapInterface::CONFIGURATION_LOADER_KEYS => $loaderKeys],
            $keyMapConfiguration
        );
        /** @var \Soong\KeyMap\KeyMapInterface $keyMap */
        $keyMap = new $taskConfiguration['key_map']['class']($keyMapConfiguration);
        return $keyMap;
    }

    /**
     * Perform an ETL migration operation.
     *
     * @param array $options
     */
    public function migrate(array $options)
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return;
        }
        $recordClass = $taskConfiguration['record_class'];
        /** @var \Soong\Extractor\ExtractorInterface $extractor */
        $extractor = new $taskConfiguration['extract']['class']($taskConfiguration['extract']['configuration']);
        $extractorKeys = $extractor->getKeyProperties();

        /** @var \Soong\Loader\LoaderInterface $loader */
        $loader = new $taskConfiguration['load']['class']($taskConfiguration['load']['configuration']);
        $loaderKeys = $loader->getKeyProperties();

        $keyMapConfiguration = $taskConfiguration['key_map']['configuration'] ?? [];
        if (!empty($keyMapConfiguration)) {
            $keyMapConfiguration = array_merge(
              [KeyMapInterface::CONFIGURATION_EXTRACTOR_KEYS => $extractorKeys],
              $keyMapConfiguration
            );
            $keyMapConfiguration = array_merge(
              [KeyMapInterface::CONFIGURATION_LOADER_KEYS => $loaderKeys],
              $keyMapConfiguration
            );
            /** @var \Soong\KeyMap\KeyMapInterface $keyMap */
            $keyMap = new $taskConfiguration['key_map']['class']($keyMapConfiguration);
        }

        /** @var \Soong\Data\DataRecordInterface $data */
        foreach ($extractor->extractFiltered() as $data) {
            /** @var \Soong\Data\DataRecordInterface $resultData */
            $resultData = new $recordClass();
            if (isset($taskConfiguration['transform'])) {
                foreach ($taskConfiguration['transform'] as $property => $transformerList) {
                    // Shortcut for directly mapping properties.
                    // @todo: Customize the default transformer class.
                    if (is_string($transformerList)) {
                        $source = $transformerList;
                        $transformerList = [
                            [
                                'class' => 'Soong\Transformer\Copy',
                                'source' => $source,
                            ],
                        ];
                    }
                    foreach ($transformerList as $transformerConfiguration) {
                        /** @var \Soong\Transformer\TransformerInterface $transformer */
                        $transformer = new $transformerConfiguration['class'];
                        $currentData = isset($transformerConfiguration['source'])
                          ? $data->getProperty($transformerConfiguration['source'])
                          : $resultData->getProperty($property);
                        $resultData->setProperty(
                            $property,
                            $transformer->transform(
                                $transformerConfiguration,
                                $currentData
                            )
                        );
                    }
                }
            }
            $loader->load($resultData);
            // @todo Handle multi-column keys.
            $extractedKey = $data->getProperty(array_keys($extractor->getKeyProperties())[0])->getValue();
            $loadedKey = $resultData->getProperty(array_keys($loader->getKeyProperties())[0])->getValue();
            if (isset($keyMap)) {
                $keyMap->saveKeyMap([$extractedKey], [$loadedKey]);
            }
        }
    }

    /**
     * Rollback an ETL migration operation.
     *
     * @param array $options
     */
    public function rollback(array $options)
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return;
        }
        /** @var \Soong\Extractor\ExtractorInterface $extractor */
        $extractor = new $taskConfiguration['extract']['class']($taskConfiguration['extract']['configuration']);
        $extractorKeys = $extractor->getKeyProperties();

        /** @var \Soong\Loader\LoaderInterface $loader */
        $loader = new $taskConfiguration['load']['class']($taskConfiguration['load']['configuration']);
        $loaderKeys = $loader->getKeyProperties();

        $keyMapConfiguration = $taskConfiguration['key_map']['configuration'] ?? [];
        $keyMapConfiguration = array_merge(
            [KeyMapInterface::CONFIGURATION_EXTRACTOR_KEYS => $extractorKeys],
            $keyMapConfiguration
        );
        $keyMapConfiguration = array_merge(
            [KeyMapInterface::CONFIGURATION_LOADER_KEYS => $loaderKeys],
            $keyMapConfiguration
        );
        /** @var \Soong\KeyMap\KeyMapInterface $keyMap */
        $keyMap = new $taskConfiguration['key_map']['class']($keyMapConfiguration);

        foreach ($keyMap->iterate() as $extractedKey) {
            $loadedKey = $keyMap->lookupLoadedKey($extractedKey);
            if (!empty($loadedKey)) {
                $loader->delete($loadedKey);
                $keyMap->delete($extractedKey);
            }
        }
    }
}
